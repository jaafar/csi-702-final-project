%==============================================================================
% FINAL -- REPORT
%==============================================================================
% CSI 702
%==============================================================================
% April 28, 2019
%==============================================================================
% JAAFAR ANSARI
%==============================================================================
\documentclass[12pt]{article}
\usepackage{amsmath}
\usepackage{color}
%\usepackage{fontspec}
\usepackage{fouriernc}
\usepackage[margin = 1in]{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{microtype}
\usepackage{subcaption}
%\usepackage{unicode-math}
\usepackage{xparse}

%------------------------------------------------------------------------------
% MISCELLANEOUS
%------------------------------------------------------------------------------
% Fonts
%\setmainfont{TeX Gyre Schola}
%\setmathfont{STIX Math}

% Listings package settings
\lstdefinelanguage{none}{   identifierstyle= }
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  firstnumber=1,                % start line enumeration with line 1000
  frame=single,                       % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=C++,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

%------------------------------------------------------------------------------
% MACROS
%------------------------------------------------------------------------------
\NewDocumentCommand{\Fig}{m}%
{%
    Figure~\ref{fig:#1}%
}

\NewDocumentCommand{\fig}{m}%
{%
    Fig.~\ref{fig:#1}%
}

\NewDocumentCommand{\figs}{mm}%
{%
    Fig.~\ref{fig:#1}-\ref{fig:#2}%
}

\NewDocumentCommand{\Listing}{m}%
{%
    Listing~\ref{listing:#1}%
}

\NewDocumentCommand{\Cpp}{}
{%
    C\nolinebreak\hspace{-.05em}%
    \raisebox{.4ex}{\tiny\bf +}\nolinebreak\hspace{-.10em}%
    \raisebox{.4ex}{\tiny\bf +}%
}

\NewDocumentCommand{\BigO}{m}
{%
    \mathcal{O}\left(#1\right)%
}

%------------------------------------------------------------------------------
% DOCUMENT %
%------------------------------------------------------------------------------
\begin{document}

    {%
        \centering%
        {%
            \Large%
            Parallelizing the Fitness Function of a Genetic Algorithm
        }

        {%
            \large%
            Jaafar Ansari,

            CSI 702 Final Project%

            May 08, 2019
        }

        \vspace{\baselineskip}

    }

    \begin{abstract}
        A serial implementation of a fitness function for a genetic algorithm
        is parallelized on a GPU.
        The function is parallelized through the CUDA and Thrust APIs
        in order to improve the performance of the function
        for large number of calls (simulating an actual genetic algorithm).
        Better than linear scaling was not seen,
        however the GPU implementation saw, on average, approximately
        500 time speedups.
    \end{abstract}

    \section{Introduction}
    The fitness function of a genetic algorithm
    is the most essential component of the algorithm due to the fact
    that it is the decision-maker on which values of the problem parameters
    are the best for the context of the problem.
    Calculation of a fitness value occurs once for each generation
    and each member of the population in that generation,
    hence the number of function calls can be large
    for a large parameter space.
    This project aims to take the fitness function for a specific problem
    where its computation is unacceptably slow
    and improve its performance by parallelization on an Nvidia GPU
    using the CUDA and Thrust APIs.

    \subsection{Background}
    The original problem that this project
    hopes to improve the implementation of is defined as follows:
    given a cylindrical electromagnetic coil
    made up of a given number of circular wires $ N $
    and a particular 3-D region outside the coil in which magnetic field
    measurement will be taken (referred to as the ``null region''),
    determine the wire positions on the coil that will produced the smallest
    radial component of the magnetic field in the null region.

    This problem was originally solved
    using a traditional nonlinear optimization routine.
    Although the solution was found, achieving the solution was difficult
    and due to the performance of the routine and the inherent difficulty
    in initializing the parameters for such routines.
    As a remedy, a genetic algorithm was attempted
    to solve this exact problem,
    with the $ N $ wire positions used as the ``chromosome''
    and the optimization being the minimization of the radial field.
    The algorithm was written in MATLAB and run serially and
    was ultimately a failure,
    presumably due to the inability to reach any reasonable
    number of generations in an acceptable amount of time.
    It was determined that the fitness function was the main bottleneck
    in that implementation.
    The project described in this report is inspired from the problems
    encountered in the original serial implementation of the genetic algorithm.

    \subsection{Fitness Function}
    The fitness function is calculated by first computing
    the radial and axial components of the magnetic field, $ B_r $ and $ B_z $,
    in the null region, computing homogeneity (defined later on) of the field,
    and then determining a fitness value from the homogeneity.

    By the principle of superposition,
    the field of the coil is found by summing the field due to a circular wire
    for each wire on the coil.
    This field \cite{smythe} is given in general by
    \begin{equation}
        B_{r_1} = \frac{\mu_0 I}{2 \pi}
              \frac{z}{r \sqrt{(a + r)^2 + z^2}}
              \left(
                  -K(k^2) + \frac{a^2 + r^2 + z^2}{(a - r)^2 - z^2} E(k^2)
              \right),
    \end{equation}
    \begin{equation}
        B_{z_1} = \frac{\mu_0 I}{2 \pi}
              \frac{1}{\sqrt{(a + r)^2 + z^2}}
              \left(
                  K(k^2) + \frac{a^2 - r^2 - z^2}{(a - r)^2 - z^2} E(k^2)
              \right)
    \end{equation}
    where $ r $ and $ z $ are positions in cylindrical coordinates,
    $ \mu_0 $ is the permeability of free space,
    $ a $ is the radius of the wire,
    and $ K $ and $ E $ are the complete elliptic integrals
    of the first and second kind, respectively.
    (The 1 subscript is present
    to remind the reader that this is the field just for a single wire
    and not the entire coil.)
    The argument for the integrals is given by
    $ k^2 = 4 a r/[(a + r)^2 + z^2] $.

    Once the field is computed, fitness is determined by homogeneity
    in $ B_r $, or how ``flat`` the field is in the null region,
    which, since the ideal case would be a field of 0 strength,
    is just the maximum value.
    Then, if the worst homogeneity in the population is $ h_\text{max} $
    and the best is $ h_\text{min} $,
    given a population member's homogeneity $ h_i $,
    we define its fitness as
    \begin{equation}
        f_i = \frac{h_\text{max} - h_i}{h_\text{max} - h_\text{min}}.
    \end{equation}
    This must be computed for each population member
    and returned to the main body of the genetic algorithm,
    each generation.
    Improving the performance of this function
    will allow the genetic algorithm to be generally more performant.

    \section{Optimizations}
    The optimizations utilized made use
    of the data-parallelism-optimized-architecture that a GPU has to offer.
    This so-called single-instruction-multiple-thread (SIMT) computing model
    is very useful since the arrays utilized to solve the problem
    are very large and can be acted-on simultaneously by many threads.
    The details of the solution are explained in detail in this section.

    The fitness function desired
    should be able to
    accept the positions defining the null region,
    a set of different wire configurations (the ``population'')
    and return an array of normalized fitness values,
    one for each member of the population.
    This is done by first initializing the spacial positions,
    population, and fitness arrays.
    This may be done in serial and on the host since
    it is only performed once.
    All magnetic field arrays and other book-keeping arrays
    are initialized from within the fitness function.
    Additionally, all vectors in the fitness function are device vectors.

    \subsection{Parallelization of the Field Computation}
    The fitness function itself is a host function,
    which makes calls to the CUDA kernels and Thrust algorithms.
    In addition, it consists of one loop over the population size;
    that is, it is not parallelized over the population,
    rather the steps to compute a single fitness value
    are parallelized.

    Therefore, the first step
    in calculating a single population member's fitness value
    is to determine the magnetic field due to specific wire positions.
    It was determined that this was best done via a custom CUDA kernel.
    The kernel accepts the spatial positions of the null region,
    wire positions, field vectors.
    The field vectors are a single contiguous array
    where each element refers to the field value at every point
    due to each wire.
    That is, if there are $ n $ data points in the positions vector
    and $ N $ wires, the first $ N $ elements refer to the first data point
    in the positions vector, for each wire.
    Visually, the indices of such a vector will look like the following:
    \begin{equation}
        [\underbrace{0, 1, \ldots,  N-1}_{n[0]},
               \underbrace{N, N+1, \ldots, 2N-1}_{n[1]}, \ldots].
    \end{equation}
    So, both field component vectors will have $ n N $ elements.
    The kernel will then compute the field value at each point,
    in parallel.
    The kernel is called with 256 threads per block
    and $ (n + 255)/256 $ blocks.
    \begin{figure}[htbp]
    \begin{lstlisting}[caption=Magnetic field computation kernel,
                       label=listing:bfield]
__global__ void B_field(...){ 
  int index = blockIdx.x*blockDim.x + threadIdx.x; // Absolute index in array
  int i = index%n; // Data point
  int w_i = index/n; // Wire number

  if (i >= n || w_i >= N) return;

  // Constants and placeholders
  double r = r_array[i]; // Radial position
  double z = z_array[i] - wires[w_i]; // Axial position
  .
  .
  .
  // Calculate field components.
  Br[index] = ...
  Bz[index] = ...
  indices[index] = i;
}\end{lstlisting}
    \end{figure}
    Also, a critical component of this kernel is the filling
    of the \texttt{indices} vector.
    This vector is also of size $ n N $;
    its elements are the index of the position vector element corresponding
    to the same element in \texttt{Br} and \texttt{Bz}.
    In other words, \texttt{indices} is a keys vector,
    which is important for reduction in the next step.

    The results from the kernel and the use of Thrust algorithms
    make the rest of calculation straight forward.
    Using the field vectors and the \texttt{indices} vector
    generated in the kernel,
    a reduction-by-key algorithm from the Thrust library can be used,
    with the field vectors
    (which are already sorted by key due to the way the threads
    were ID'd in the kernel; see Lines 2-3 in \Listing{bfield}) as values
    and \texttt{indices} as keys.
    Preallocating new field vectors of size $ n $ at this point,
    the reduction is performed with
    \texttt{thrust::reduce\_by\_key()}.
    The last step is then to calculate the field homogeneity
    by a similar Thrust algorithm, \texttt{thrust::transform\_reduce},
    which takes the absolute value of the radial field
    and returns the maximum value by way of a functor.

    At this point, one population member's fitness homogeneity has been found.
    The rest must be found and placed into a homogeneity vector
    so that the fitnesses for each member can be found.

    \subsection{Fitness Calculation}
    This step in the function cannot be performed until
    all homogeneity values are calculated since
    each fitness calculation depends on the maximum and minimum
    homogeneity values in the population.
    The maximum and minimum elements of the homogeneity vector
    are found through Thrust algorithms \texttt{max\_element}
    \texttt{min\_element}.
    The fitness values are then determined via a custom functor,
    shown in \Listing{fitness_functor}
    \begin{figure}[htbp]
    \begin{lstlisting}[caption=Fitness calculation functor,
                       label=listing:fitness_functor]
struct hg_to_fit{
  double best;
  double worst;

  __host__ __device__
  hg_to_fit(double best, double worst) : best(best), worst(worst) {}

  __host__ __device__
  double operator()(double h) {
    return (worst - h)/(worst - best);
  }
};\end{lstlisting}
    \end{figure}
    The functor is called via Thrust's \texttt{transform} algorithm,
    which will accept the homogeneity vector and output
    the fitness values for all population members.
    The fitness vector will at this point be returned
    to the main body of the genetic algorithm,
    where its values will be used for the selection portion
    of the algorithm.

    \section{Correctness}
    The correctness of the magnetic field from the coil
    is verified by confirming that each coil produces the correct field
    at the center of the coil,
    which is given by $ \mu_0 I/(2 a) $ \cite{griffiths}.
    The code returned \texttt{4.46011e-05}, when $ r $ and $ z $
    are set to 0, as expected.

    \section{Benchmarks}
    Benchmarks were performed for both the GPU and serial code,
    with the serial code serving as the baseline for measuring speedups.
    Both programs were run on Bridges computing cluster.
    The P100 GPU was used for the GPU code
    and a single ``Bridges regular'' node was used for the serial code.
    Timing was performed only on the fitness function calls.
    The GPU code was timed using the chrono library
    available in the \Cpp{} Standard Library
    while the serial code was timed using the \texttt{tic toc} function calls
    provided in MATLAB.

    In \fig{pop_benchmarks}, we see how the execution time
    of the fitness function changes with the population size
    in a given generation.
    Runtime changes linearly, both in serial and parallel versions.
    This is not surprising due to the way the fitness function
    was constructed.
    The main \texttt{for} loop in the function is very uniform
    in that each step is its own parallelized region
    via a kernel or Thrust call.
    However, a perhaps more significant detail is the large speedups.
    On average, the GPU performed almost 500 times faster than the serial code.
    Although the serial code was vectorized
    according to the available data structures in MATLAB,
    the GPU shows how it is able to handle large vectors
    and have many threads operating on them simultaneously.
    \begin{figure}[hpbt]
        \centering
        \begin{subfigure}{0.48\textwidth}
            \centering%
            \includegraphics[width = \textwidth]{./img/serial_pop.pdf}%
            \caption{Serial performance}%
            \label{fig:pop_benchmarks:serial}%
        \end{subfigure}
        \begin{subfigure}{0.48\textwidth}
            \centering%
            \includegraphics[width = \textwidth]{./img/gpu_pop.pdf}%
            \caption{Parallel performance}%
            \label{fig:pop_benchmarks:gpu}%
        \end{subfigure}
        \caption{Performance of the fitness function
                 as population size increases.}
        \label{fig:pop_benchmarks}
    \end{figure}

    Changing the population size is not scaling up the problem size
    in the context of the problem at hand.
    To scale up the coil problem would entail increasing the number of
    wires on the coil.
    However, the results will not be too different from what was shown
    in increasing the population size because the population size
    is dependent upon the number of parameters in the given problem.
    Namely, it was decided for this problem to keep
    the population size proportional to half the number of wires
    (since the coil is symmetric,
    only half the wire positions will be optimized in an actual
    genetic algorithm implementation).
    As a result, we see approximately linear scaling with number of wires,
    as shown in \fig{wires_benchmarks}.
    Interestingly, the average speedup was found be under 400 times.
    \begin{figure}[hpbt]
        \centering
        \begin{subfigure}{0.48\textwidth}
            \centering%
            \includegraphics[width = \textwidth]{./img/serial_wires.pdf}%
            \caption{Serial performance}%
            \label{fig:wires_benchmarks:serial}%
        \end{subfigure}
        \begin{subfigure}{0.48\textwidth}
            \centering%
            \includegraphics[width = \textwidth]{./img/gpu_wires.pdf}%
            \caption{Parallel performance}%
            \label{fig:wires_benchmarks:gpu}%
        \end{subfigure}
        \caption{Performance of the fitness function
                 as number of wires increase.}
        \label{fig:wires_benchmarks}
    \end{figure}

    Despite the massive improvements in parallelizing
    the calculation of the magnetic fields and homogeneity,
    there is still plenty that can be done to even further improve
    the performance and make the scaling better than linear.
    This can be done by parallelizing the main \texttt{for} loop
    in the fitness function,
    which should be possible because as it is currently written,
    it is already embarrassingly parallel;
    the fitness value of each member in the population
    can be calculated independently of the other.
    This level of parallelization remains to be implemented and tested,
    but it is certainly worth further investigation.

    \clearpage

    \bibliography{bib}
    \bibliographystyle{unsrt}

\end{document}

