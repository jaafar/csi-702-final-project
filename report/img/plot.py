import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys
sys.path.insert(0, '../../scripts/')
from benchmarker import read_summary_file
from benchmarker import find_loglog_slope

df = read_summary_file('../data/serial_pop_sum.txt', '../data/gpu_pop_sum.txt')
x = df.loc[df.benchmark_type == 'gpu', 'popsize']
y = df.loc[df.benchmark_type == 'gpu', 'runtime']
plt.plot(x, y)
plt.xlabel("Size of population", fontsize=16)
plt.ylabel('Time (s)', fontsize=16)
ax = plt.gca()
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
plt.savefig("gpu_pop.pdf", bbox_inches='tight')
plt.clf()
x = df.loc[df.benchmark_type == 'serial', 'popsize']
y = df.loc[df.benchmark_type == 'serial', 'runtime']
plt.plot(x, y)
plt.xlabel("Size of population", fontsize=16)
plt.ylabel('Time (s)', fontsize=16)
ax = plt.gca()
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
plt.savefig("serial_pop.pdf", bbox_inches='tight')
print("Speedup (popsize):",
      np.average(df.loc[df.benchmark_type == "serial", 'runtime']/df.loc[df.benchmark_type == 'gpu', 'runtime']))

plt.clf()

df = read_summary_file('../data/serial_wires_sum.txt', '../data/gpu_wires_sum.txt')
x = df.loc[df.benchmark_type == 'gpu', 'wires']
y = df.loc[df.benchmark_type == 'gpu', 'runtime']
plt.plot(x, y)
plt.xlabel("Number of wires", fontsize=16)
plt.ylabel('Time (s)', fontsize=16)
ax = plt.gca()
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
plt.savefig("gpu_wires.pdf", bbox_inches='tight')
plt.clf()
x = df.loc[df.benchmark_type == 'serial', 'wires']
y = df.loc[df.benchmark_type == 'serial', 'runtime']
plt.plot(x, y)
plt.xlabel("Number of wires", fontsize=16)
plt.ylabel('Time (s)', fontsize=16)
ax = plt.gca()
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
plt.savefig("serial_wires.pdf", bbox_inches='tight')
print("Speedup (wires):",
      np.average(df.loc[df.benchmark_type == "serial", 'runtime']/df.loc[df.benchmark_type == 'gpu', 'runtime']))

