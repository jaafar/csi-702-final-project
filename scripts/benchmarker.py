#!/usr/bin/env python
import argparse
import numpy as np
import pandas as pd
import statsmodels.formula.api as smf

def read_summary_file(serial_file, gpu_file):
    serial_df = pd.read_table(serial_file, sep="\s+", header=None,
                              names=["popsize", "wires", "runtime"])
    serial_df["benchmark_type"] = "serial"

    gpu_df = pd.read_table(gpu_file, sep="\s+", header=None,
                           names=["popsize", "wires", "runtime"])
    gpu_df["benchmark_type"] = "gpu"

    benchmarks_df = pd.concat([serial_df, gpu_df])

    return benchmarks_df


def find_loglog_slope(df):
    gpu_df = df.query("benchmark_type == 'gpu'")
    slope = smf.ols("np.log10(runtime) ~ np.log10(gens)",
                    data=gpu_df).fit().params[1]

    return slope


def compute_speedup(df):
    gpu_df = df.query("benchmark_type == 'gpu'").reset_index(drop=True)
    serial_df = df.query("benchmark_type == 'serial'").reset_index(drop=True)

    speedup_series = serial_df["runtime"] / gpu_df["runtime"]

    speedup_df = pd.DataFrame(
        {"gens": gpu_df["gens"], "speedup": speedup_series}
    )

    return speedup_df


def build_results_table(speedup_df: pd.DataFrame) -> str:
    """Generate human-readable table of the speedup results.

    Parameters
    ----------
    speedup_df : pd.DataFrame
        Data frame of GPU speedups relative to O(n) serial runs.

    Returns
    -------
    tuple
        Generated strings for the speedup table.
    """
    speedup_table: str = (
        "| Number of particles | Speed-up |\n" "| ------------------- | -------- |\n"
    )

    speedup_df = speedup_df.copy()
    speedup_df["row_text"] = (
        "| "
        + speedup_df["particles"].apply(lambda x: f"{x:^19.0f}")
        + " | "
        + speedup_df["speedup"].apply(lambda x: f"{x:^8.4f}")
        + " |\n"
    )

    for table_row in speedup_df["row_text"]:
        speedup_table += table_row

    return speedup_table


def grade_runs(benchmarks_df: pd.DataFrame) -> float:
    """Grade runs and print results.

    Parameters
    ----------
    benchmarks_df: Benchmarks
        Data frame of benchmark runs.

    Return
    ------
    grade: float
        Grade for assignment.
    """
    speedup_df: pd.DataFrame = compute_speedup(benchmarks_df)
    loglog_slope: float = find_loglog_slope(benchmarks_df)

    speedup_grade: float = apply_speedup_grading_schema(speedup_df)
    scaling_grade: float = apply_scaling_grading_schema(loglog_slope)

    speedup_table = build_results_table(speedup_df)

    print("\nScaling results:")
    print("O(n^m) log-log slope: m = {0:.4f}\n".format(round(loglog_slope, 4)))

    print("\nSpeedup results:")
    print(speedup_table)
    print("Average speedup: {0:.4f}\n\n".format(round(speedup_df["speedup"].mean(), 4)))

    grade: float = speedup_grade + scaling_grade

    return grade


def autograde_hw2c(summary_file: str) -> None:
    """Autograde summary file.

    Parameters
    ----------
    summary_file : str
        File containing particle counts and simulation run-times.

    """
    benchmarks_df: pd.DataFrame = read_summary_file(summary_file)
    grade: float = grade_runs(benchmarks_df)

    print("Grade = {0:.2f}\n".format(grade))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Autograder for Homework 2, Part C.")
    parser.add_argument(
        "summaryfile",
        type=str,
        help="Summary file for simulation runs over different particle counts.",
    )
    args = parser.parse_args()

    autograde_hw2c(summary_file=args.summaryfile)
