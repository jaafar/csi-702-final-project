#include <chrono>
#include <iostream>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/transform.h>
#include <thrust/tuple.h>

#include "coil.cuh"
#include "fit.cuh"
#include "genetic.cuh"

namespace genetic {

void genetic(coil::cli_parameters_t &cli_parameters) {
  // Initialize parameters
  double N = cli_parameters.num_wires; // Number of wires
  int d = cli_parameters.grid_density; // Grid density (per meter)
  double l = coil::region_length; // Length of region of measurement (x)
  double w = coil::region_width; // Width of region of measurement (y)
  double h = coil::region_height; // Height of region of measurement (z)
  int l_n = d*l; // Number of data points in length axis
  int w_n = d*w; // Number of data points in width axis
  int h_n = d*h; // Number of data points in height axis
  int n = l_n*w_n*h_n; // Total number of data points
  int P; // Population size
  if (cli_parameters.pop_size == 0){
    P = 5*N;
  } else{
    P = cli_parameters.pop_size;
  }

  // Initialize host and device vectors
  thrust::host_vector<double> r_h(n);
  thrust::host_vector<double> phi_h(n);
  thrust::host_vector<double> z_h(n);
  thrust::host_vector<double> wires_h(N);
  thrust::host_vector<double> pop_h(P*N);
  thrust::host_vector<double> fits_h(P);
  thrust::device_vector<double> r(n);
  thrust::device_vector<double> phi(n);
  thrust::device_vector<double> z(n);
  thrust::device_vector<double> pop(P*N);
  thrust::device_vector<double> fits(P);

  // Initialize the uniform wire positions
  init_wires(N, wires_h);

  // Initialize the population uniformly (wires_h repeated P times)
  init_pop(N, P, pop_h, wires_h);

  // Initialize the grid space in cylindrical coordinates (absolute)
  init_grid(d, r_h, phi_h, z_h, wires_h);

  // Load all necessary vectors onto device
  r = r_h;
  z = z_h;
  pop = pop_h;

  // Time fitness function
  auto start = std::chrono::high_resolution_clock::now();

  // Calculate fitness of each chromosome in the population
  fit::fitness(N, n, r, z, pop, fits);

  auto stop = std::chrono::high_resolution_clock::now();

  // Outputs and writes
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>
                    (stop - start);
  std::cout << P << " " << N << " " << duration.count()/1e6 << "\n";
}

// Initialize the grid of the region of measurement.
void init_grid(int d,
               thrust::host_vector<double> &r,
               thrust::host_vector<double> &phi,
               thrust::host_vector<double> &z,
               thrust::host_vector<double> &wires){
  double x, y, z_val;
  double l = coil::region_length;
  double w = coil::region_width;
  double h = coil::region_height;
  int l_n = d*l;
  int w_n = d*w;
  int h_n = d*h;
  double step_l = l/(static_cast<double>(l_n)-1);
  double step_w = w/(static_cast<double>(w_n)-1);
  double step_h = h/(static_cast<double>(h_n)-1);
  int N = wires.size();

  z_val = -h/2;
  for (int k = 0; k<h_n; k++){
    y = -w/2;
    for (int j = 0; j<w_n; j++){
      x = coil::a + 1e-2; // Start at 1cm out from edge of cylinder
      for (int i = 0; i<l_n; i++){
        int index = k*w_n*l_n + j*l_n + i;
        // Convert to cylindrical coordinates (no theta dependence)
        r[index] = sqrt(x*x + y*y);
        phi[index] = atan2(y, x)*180.0/coil::pi;
        z[index] = z_val;
        x += step_l;
      }
      y += step_w;
    }
    z_val += step_h;
  }
}

// Initialize the population (made up of different wire configurations, ideally)
void init_pop(int N, int P, thrust::host_vector<double> &p,
              thrust::host_vector<double> &w){
  for (int i = 0; i<P; i++){
    thrust::copy_n(w.begin(), N, p.begin() + i*N);
  }
}

// Initialize the wire positions on the coil uniformly
void init_wires(int N, thrust::host_vector<double> &w){
  double step, position;
  double L = coil::L;
  if (N == 1){
    step = 0;
    position = 0;
  } else{
    step = L/(N-1);
    position = -L/2;
  }

  for (int i = 0; i<N; i++){
    w[i] = position;
    position += step;
  }
}

} // genetic namespace
