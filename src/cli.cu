#include <CLI11.hpp>
#include <string>

#include "coil.cuh"
#include "genetic.cuh"

int main(int argc, char **argv) {

  // This takes a few seconds to initialize the runtime
  cudaThreadSynchronize();

  CLI::App app;
  coil::cli_parameters_t cli_parameters = {};

  double grid_density_option = 5;
  app.add_option("-d", grid_density_option,
                 "Set the grid density (per centimeter)"
                 " for the region of measurement", true);

  int wires_option = 190;
  app.add_option("-n", wires_option,
                 "Set the total number of wires (must be even, for symmetry)",
                 true);

  int popsize_option = 0;
  app.add_option("-p", popsize_option,
                 "Set the size of the population",
                 true);

  CLI11_PARSE(app, argc, argv);

  cli_parameters.grid_density = static_cast<int>(grid_density_option)*100;
  cli_parameters.num_wires = wires_option;
  cli_parameters.pop_size = popsize_option;

//  if (wires_option%2 == 0){
    genetic::genetic(cli_parameters);
//  } else {
//    std::cout << "error: " << wires_option << " is not even."
//              << " Please enter an even number.\n"
//              << "The coil is symmetric and so it will have the same"
//              << " number of wires in the top and bottom half."
//              << "\n";
//  }

  return 0;
}
