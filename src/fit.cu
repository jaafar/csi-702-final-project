#include <thrust/device_vector.h>
#include <thrust/reduce.h>

#include "coil.cuh"
#include "fit.cuh"

#define NUM_THREADS 256

namespace fit{

__global__
void B_field(int N, int n,
             double *r_array, double *z_array,
             double *Br, double *Bz, double *wires,
             int *indices){
  int index = blockIdx.x*blockDim.x + threadIdx.x; // Absolute index in array
  int i = index%n; // Data point
  int w_i = index/n; // Wire number

  if (i >= n || w_i >= N) return;

  double pi = coil::pi;
  double mu0 = coil::mu0;
  double I = coil::I;
  double a = coil::a;
  double L = coil::L;
  double norm = mu0*static_cast<double>(N)/L*I;

  // Equations are pulled from Smythe textbook (see report references).
  double K, E;
  double r = r_array[i];   // Radial component
  double z = z_array[i] - wires[w_i];   // Axial component
  double c = mu0 * I / 2 / pi;
  double apr2 = (a + r)*(a + r);
  double amr2 = (a - r)*(a - r);
  double den = sqrt(apr2 + z*z);
  double k2 = 4 * a * r / (apr2 + z*z); // Elliptic integral arg
  fit::ellintKE(k2, K, E); // Complete elliptic integrals

  // Calculate field components.
  // Radial component is normalized to field of infinitely long solenoid
  Br[index] = c * z / r / den * (-K + (a*a + r*r + z*z) * E / (amr2 + z*z))
                                                                          /norm;
  Bz[index] = c * 1 / den * (K + (a*a - r*r - z*z) * E / (amr2 + z*z));
  indices[index] = i;
}

__host__
void fitness(int N, int n,
             thrust::device_vector<double> &r,
             thrust::device_vector<double> &z,
             thrust::device_vector<double> &pop,
             thrust::device_vector<double> &fits){

  int P = pop.size()/N; // Size of population

  // Initialize vectors not needed on host
  thrust::device_vector<double> Br_all(n*N);
  thrust::device_vector<double> Bz_all(n*N);
  thrust::device_vector<double> Br(n);
  thrust::device_vector<double> Bz(n);
  thrust::device_vector<int> indices_all(n*N);
  thrust::device_vector<int> indices(n);
  thrust::device_vector<double> wires(N);
  thrust::device_vector<double> hg(P);

  for (int i = 0; i<P; i++){
    // Copy current population member into separate vector
    thrust::copy_n(pop.begin()+i*N, N, wires.begin());

    // Calculate magnetic field at each point for each wire
    int blks = (n + NUM_THREADS - 1)/NUM_THREADS;
    fit::B_field<<<blks, NUM_THREADS>>>(
                                    N, n,
                                    thrust::raw_pointer_cast(r.data()),
                                    thrust::raw_pointer_cast(z.data()),
                                    thrust::raw_pointer_cast(Br_all.data()),
                                    thrust::raw_pointer_cast(Bz_all.data()),
                                    thrust::raw_pointer_cast(wires.data()),
                                    thrust::raw_pointer_cast(indices_all.data())
    );

    // Add all the field components due to each wire at each point
    thrust::reduce_by_key(indices_all.begin(), indices_all.end(),
                          Br_all.begin(),
                          indices.begin(),
                          Br.begin());
    thrust::reduce_by_key(indices_all.begin(), indices_all.end(),
                          Bz_all.begin(),
                          indices.begin(),
                          Bz.begin());

    // Calculate field homogeneity
    hg[i] = thrust::transform_reduce(Br.begin(), Br.end(),
                                       abs_val<double>(), 0.0,
                                       thrust::maximum<double>());
  }

  // Calculate fitness values from homogeneity
  thrust::device_vector<double>::iterator worst =
                                  thrust::max_element(hg.begin(), hg.end());
  thrust::device_vector<double>::iterator best =
                                  thrust::min_element(hg.begin(), hg.end());
  thrust::transform(hg.begin(), hg.end(),
                    fits.begin(), hg_to_fit(*best, *worst));
}

// Elliptic integral calculation.
// Retrieved from MATLAB source.
// Accessed via MATLAB interface using command `edit ellipke`.
// https://www.mathworks.com/help/matlab/ref/ellipke.html
__device__
void ellintKE(double m, double &K, double &E){
  double a, g, c, w;
  double tol = 1e-16;
  double a0 = 1;
  double g0 = sqrt(1 - m);
  double s0 = m;
  double err = 1;
  double i = 0;
  while (err > tol){
    i++;
    a = (a0 + g0)/2;
    g = sqrt(a0*g0);
    c = (a0 - g0)/2;
    w = pow(2, i)*c*c;
    err = w;
    a0 = a;
    g0 = g;
    s0 = s0 + w;
  }
  K = coil::pi/2/a;
  E = K*(1 - s0/2);
}

} // fit namespace
