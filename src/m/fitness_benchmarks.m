%==========================================================================
% Fitness Benchmarks
%==========================================================================
function []=fitness_benchmarks(P, N, file)
% P: population size
% N: number of wires

global a mu0 L I N xRange yRange zRange
%% Coil Parameters
a = (1 + 1 / 8) * 0.0254 / 2 - 0.2e-3; % Cylinder radius (m)
L = 42e-2; % Length of coil
mu0 = pi*4e-7; % Permeability of free space
I = 1;
density = 5*100;
length = 2e-2;
width = 2.5e-2;
height = 6.5e-2;
xRange = linspace(a + 1e-2, a + 1e-2 + length, floor(density*length));
yRange = linspace(-width/2, width/2, floor(density*width));
zRange = linspace(-height/2, height/2, floor(density*height));

pop = ones(P, N);
for i=1:P
    pop(i, :) = linspace(-L/2, L/2, N);
end

tic
fits = Fitness(pop);
toc
%f = @() Fitness(pop);
%t = timeit(f);

fileID = fopen(file, 'a');
fprintf(fileID, "%f  %f  %f\n", P, N, toc);

end

% Functions
function fitness = Fitness(pop)
    global xRange yRange zRange

    popsize = size(pop);
    for i = 1:popsize(1)
        [Br, Bz] = BField(xRange, yRange, zRange, pop(i, :));

        hg(i) = max(max(max(abs(Br))));
    end

    best = min(hg);
    worst = max(hg);
    fitness = ones(size(hg));
    for i = 1:length(hg)
        fitness(i) = (worst - hg(i))/(worst - best);
    end
 end

function [Br,Bz] = BField(x, y, z, wires)
    global a mu0 I N L

    [X, Y, Z] = meshgrid(x, y, z);

    R = sqrt(X.^2 + Y.^2);

    Br = zeros(size(R));
    Bz = zeros(size(Z));
    for i = 1:length(wires)
        Zp = Z - wires(i);
        k_sq = 4.*a.*R./((a + R).^2 + Zp.^2);
        [K,E] = ellipke(k_sq); % Declare elliptic integrals
        C = mu0*I/2/pi;
        Br = Br + C.*Zp./(R.*sqrt((a+R).^2 + Zp.^2))...
                  .*(-K + (a^2 + R.^2 + Zp.^2).*E./((a - R).^2 + Zp.^2));
        Bz = Bz + C.*1./sqrt((a+R).^2 + Zp.^2)...
                  .*(K + (a^2 - R.^2 - Zp.^2).*E./((a - R).^2 + Zp.^2));
    end
    Br = Br./(mu0*N/L*I);
end
