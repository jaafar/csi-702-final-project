#ifndef FIT_CUH
#define FIT_CUH

#include "coil.cuh"

namespace fit{

__device__ void ellintKE(double m, double &K, double &E);
__global__ void B_field(int N, int n,
                        double *r_array, double *z_array,
                        double *Br, double *Bz, double *wires,
                        int *indices);
__host__ void fitness(int N, int n,
                      thrust::device_vector<double> &r,
                      thrust::device_vector<double> &z,
                      thrust::device_vector<double> &pop,
                      thrust::device_vector<double> &fits);

// Functor for converting homogenity to fitness value
struct hg_to_fit{
  double best;
  double worst;

  __host__ __device__
  hg_to_fit(double best, double worst) : best(best), worst(worst) {}

  __host__ __device__
  double operator()(double h) {
    return (worst - h)/(worst - best);
  }
};

// Absolute value functor (modified from Thrust documentation)
// (https://thrust.github.io/doc/group__transformed__reductions.html)
template<typename T>
struct abs_val{
  __host__ __device__ T operator()(const T &x) const
  {
    return x < T(0) ? -x : x;
  }
};

} // fit namespace

#endif
