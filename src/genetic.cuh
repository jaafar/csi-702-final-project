#ifndef GENETIC_CUH
#define GENETIC_CUH

#include <thrust/host_vector.h>

#include "coil.cuh"

namespace genetic{

void init_grid(int d,
               thrust::host_vector<double> &r,
               thrust::host_vector<double> &phi,
               thrust::host_vector<double> &z,
               thrust::host_vector<double> &wires);
void init_wires(int N, thrust::host_vector<double> &w);
void init_pop(int N, int P,
              thrust::host_vector<double> &p,
              thrust::host_vector<double> &w);
void genetic(coil::cli_parameters_t &cli_parameters);

}

#endif
