#ifndef COIL_CUH
#define COIL_CUH

#include <string>

namespace coil{

// Physical and coil parameter constants. (Everything stays in base SI units).
const double pi = 3.14159265358979323846; // pi
const double mu0 = 4.0 * pi * 1e-7; // Vacuum permeability
const double I = 1; // Normalized current
const double a = (1.0 + 1.0 / 8.0) * 0.0254 / 2.0 - 0.2e-3; // Coil radius
const double L = 42e-2; // Coil length

// Parameters related to the region of measurement.
const double region_height = 6.5e-2; // Parallel to axis of coil (z)
const double region_width = 2.5e-2; // Perpendicular to axis of coil (y)
const double region_length = 2e-2; // Perpendicular to radial of coil (x)

// CLI Parameters data structure
struct cli_parameters_t {
  int grid_density;
  int num_wires;
  int pop_size;
};

}

#endif
