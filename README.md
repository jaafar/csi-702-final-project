# Final Project: Parallelizing the Fitness Function of a Genetic Algorithm  
# Jaafar Ansari  
# CSI 702  

**Proposal Due Date: April 5, 2019 @ 11:59pm**  
**Due Date: May 8, 2019 @ 11:59pm**  
**Presentation Date: May 13, 2019**  

## Source code

The files are organized and named in such a way that a genetic algorithm
would most reasonably be organized, although a genetic algorithm
is not implemented.

*   **src/cli.cu** (DO NOT EDIT) and **vendor/CLI/CLI11.hpp** (DO NOT EDIT)

    * Unified interface for defining and controlling command-line options

*   **src/coil.cuh**

    * Coil parameters that can't be altered via CLI are hard-coded here

*   **src/fit.cu** and **src/fit.cuh**

    * The fitness function and supporting functions are defined here

*   **src/genetic.cu** and **src/genetic.cuh**

    * This is where the fitness function is called
      and benchmark measurements are made

*   **src/m/fitness_benchmarks.m**

    * The baseline benchmarks are taken from this matlab file,
      which is very similar to the original code that this project
      was inspired from.

*   **CMakeLists.txt** (DO NOT EDIT) and **src/CMakeLists.txt**

    * CMake configuration files for compiling your code.
      compiler flags and the full list of source files to compile
      can be edited in **src/CMakeLists.txt**.

*   **gpu-pop-benchmarks**

    * Run benchmarks on the GPU code, producing timing results as
      population size changes

*   **gpu-wires-benchmarks**

    * Run benchmarks on the GPU code, producing timing results as
      number of wires change

*   **serial-pop-benchmarks**

    * Run benchmarks on the serial code, producing timing results as
      population size changes

*   **serial-wires-benchmarks**

    * Run benchmarks on the serial code, producing timing results as
      number of wires changes.

## Compiling the code

Instead of a regular Makefile, compilation is handled via CMake.
For convenience, a simple script file named `make.sh` is included
that will automatically compile the code on Bridges.
From the root directory of this repository, simply run:

    ./make.sh

The code should compile, and the compiled binary `coil`
will be placed into a folder called `bin/`.

If you are developing on your local computer,
you can compile the code by running:

    ./make.sh nomodule

## CLI interface

The binary has no modes, it only runs on a GPU
and only certain parameters can be changed.
Running `./bin/coil -h` will bring up the help:

    Usage: ./bin/coil [OPTIONS]

    Options:
      -h,--help                   Print this help message and exit
      -d FLOAT=5                  Set the grid density (per centimeter) for the region of measurement
      -n INT=190                  Set the total number of wires (must be even, for symmetry)
      -p INT=0                    Set the size of the population

