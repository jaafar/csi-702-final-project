#!/bin/bash

echo ""
echo "To run in interact mode, execute the following:"
echo ""
echo "    interact -p GPU-shared -N 1 --gres=gpu:k80:1 --ntasks-per-node=1"
echo ""
echo "Then load the following modules:"
echo ""
echo "    module load cmake/3.11.4 gcc/5.3.0 cuda/9.2 anaconda/5.2.0"
echo ""

module load cmake/3.11.4 gcc/5.3.0 cuda/9.2 anaconda3/5.2.0

if [ "$1" == "k80" ]; then
    interact -p GPU-shared -N 1 --gres=gpu:k80:1 --ntasks-per-node=1
else
    interact -gpu
fi

